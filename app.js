const express = require('express');
const cors = require('cors');
app = express();

const port = process.env.port || 4010;

var corsOptions = {
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH'],
    origin: true, // sesuai sama
    credentials:true,   
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}


app.use(cors(corsOptions));

app.get("/", (req, res) => { 
    return res.status(200).json({msg:'success'});
 });

 app.listen(port, () => { 
    console.log(`server running on port ${port}`);
  })
